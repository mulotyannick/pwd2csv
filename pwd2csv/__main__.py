"""Command-line interface for pwd2csv utility."""

import argparse

from . import __version__, extract


def cli():
    """Parse command-line arguments and execute the conversion process."""
    parser = argparse.ArgumentParser(
        prog='pwd2csv',
        description='extract and convert network credentials from a PDF document into separate CSV files sorted by class'
    )

    parser.add_argument(
        'file',
        help='input PDF file path containing the network credentials to be converted to CSV'
    )

    parser.add_argument(
        '-o', '--output',
        help='path to the output directory for generated CSV files (default: current directory)',
        default='.',
        dest='output_dir'
    )

    parser.add_argument(
        '-v', '--version',
        action='version',
        version=__version__
    )

    args = parser.parse_args()
    extract(args.file, args.output_dir)


if __name__ == '__main__':
    cli()
