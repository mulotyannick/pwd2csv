"""PDF network credentials extractor and CSV converter."""

import csv
from pathlib import Path
from typing import List, Iterator

from pypdf import PdfReader


def extract(file: str, output_dir: str = '.') -> None:
    """Extract credentials from PDF and save them as CSV files by section.

    Args:
        file: Path to the input PDF file
        output_dir: Directory where CSV files will be saved (default: current directory)
    """
    # Create output directory if it doesn't exist
    output_path = Path(output_dir)
    output_path.mkdir(parents=True, exist_ok=True)

    # Parse input PDF file and extract user credentials by section
    sections = {}
    for row in _extract_all(file):
        if row[2] not in sections:
            sections[row[2]] = []
        sections[row[2]].append(row)

    # Generate one CSV file per section
    for section, rows in sections.items():
        # Build the full path for CSV file
        filename = output_path / f"{section.upper()}_credentials.csv"

        with open(filename, mode='w', newline='', encoding='utf-8') as file:
            writer = csv.writer(file)
            for row in rows:
                writer.writerow(row)


def _extract_all(file: str) -> Iterator[List[str]]:
    """Extract all credentials from the PDF file.

    Args:
        file: Path to the PDF file to process

    Returns:
        Iterator of credential lists [firstname, lastname, section, login, password]
    """
    reader = PdfReader(file)

    for page_number, page in enumerate(reader.pages):
        rows = page.extract_text(extraction_mode='layout').split('\n')

        if page_number == 0:
            rows = rows[3:]

        for row in rows:
            elements = [v for v in row.split(' ') if v != '']

            if len(elements) < 5:
                continue

            infos = elements[:4]
            password = ''.join(elements[4:])
            infos.append(password)

            yield infos
