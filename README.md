# Commande pwd2csv

Commande en ligne (CLI) permettant de convertir le fichier PDF des identifiants du réseau pédagogique du lycée Gaston Bachelard en fichiers CSV classés par classe.

## Prérequis

Avant l'installation, assurez-vous d'avoir les dépendances suivantes installées sur votre système :

- [`pipx`](https://pypa.github.io/pipx/) - Gestionnaire de paquets pour applications Python
- [`poetry`](https://python-poetry.org/docs/#installation) - Gestionnaire de dépendances Python

## Installation

Exécutez les commandes suivantes dans votre terminal :

```bash
# Construction du package
poetry build

# Installation de l'application
pipx install dist/pwd2csv-1.2.1.tar.gz
```